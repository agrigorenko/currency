package by.scand.hryharenka.currency.integration;

import by.scand.hryharenka.currency.domain.Cube;
import by.scand.hryharenka.currency.domain.repository.CubeRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.assertj.core.api.Assertions.assertThat;

@TestPropertySource(locations = "classpath:application-test.properties")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApplicationTest {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Autowired
    private CubeRepository cubeRepository;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;


    private Cube cubeFrom;
    private Cube cubeTo;

    @BeforeEach
    public void putEntity() {
        String stringDate = formatter.format(LocalDate.now());
        cubeFrom = new Cube();
        cubeFrom.setCurrency("USD");
        cubeFrom.setRate(new BigDecimal(1.0842));
        cubeFrom.setTime(stringDate);
        cubeRepository.save(cubeFrom);

        cubeTo = new Cube();
        cubeTo.setCurrency("PLN");
        cubeTo.setRate(new BigDecimal(4.2490));
        cubeTo.setTime(stringDate);
        cubeRepository.save(cubeTo);
    }

    @AfterEach
    public void clearDatabase(){
        cubeRepository.delete(cubeFrom);
        cubeRepository.delete(cubeTo);
    }


    @Test
    public void shouldReturnCalculatedMessage() throws Exception {
        assertThat(restTemplate.getForObject("http://localhost:" + port + "/exchange?from=USD&to=PLN&amount=50",
                String.class)).isEqualTo("{\"from\":\"USD\",\"amount\":50.0,\"to\":\"PLN\",\"result\":195.95093156244232,\"date\":\"2020-02-16\"}");
    }

    @Test
    public void shouldReturnErrorMessage() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/exchange?from=USD1&to=PLN&amount=50",
                String.class)).contains("{\"message\":\"calculating with next parameters not acceptable:{from=USD1, to=PLN, amount=50} .Please check parameters and try again!\"}");
    }
}
