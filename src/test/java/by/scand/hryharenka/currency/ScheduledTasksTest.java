package by.scand.hryharenka.currency;

import by.scand.hryharenka.currency.domain.Cube;
import by.scand.hryharenka.currency.domain.repository.CubeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;

import java.util.stream.StreamSupport;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestPropertySource(locations = "classpath:application-test.properties")
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class ScheduledTasksTest {

    private static final int CUBES_SIZE = 32;

    @Autowired
    private CubeRepository cubeRepository;

    @Autowired ScheduledTasks scheduledTasks;

    @Test
    public void shouldExecuteTaskAndSaveInDb(){
        scheduledTasks.loadAndSaveCurrencies();
        Iterable<Cube> cubes = cubeRepository.findAll();
        assertEquals(CUBES_SIZE, StreamSupport.stream(cubes.spliterator(), false).count());

    }
}
