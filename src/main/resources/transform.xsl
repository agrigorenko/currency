<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:cur="http://www.ecb.int/vocabulary/2002-08-01/eurofxref"
                xmlns:gesmes="http://www.gesmes.org/xml/2002-08-01">
    <xsl:output indent="yes" method="xml"/>
    <xsl:variable name="request_date">
        <xsl:value-of select="gesmes:Envelope/cur:Cube/cur:Cube/@time"/>
    </xsl:variable>

    <xsl:template match="gesmes:Envelope/gesmes:subject"/>
    <xsl:template match="gesmes:Envelope/gesmes:Sender"/>

    <xsl:template match="gesmes:Envelope/cur:Cube">
        <Cubes>
            <xsl:apply-templates select="@*|node()"/>
        </Cubes>
    </xsl:template>

    <xsl:template match="gesmes:Envelope/cur:Cube/cur:Cube/cur:Cube">
        <xsl:copy copy-namespaces="no">
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="time">
                <xsl:value-of select="$request_date"/>
            </xsl:attribute>
            <xsl:value-of select="."/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>