package by.scand.hryharenka.currency;

import by.scand.hryharenka.currency.domain.Cube;
import by.scand.hryharenka.currency.domain.Cubes;
import by.scand.hryharenka.currency.domain.repository.CubeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class ScheduledTasks {

    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);

    private static final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy:MM:dd:HH:mm:ss");

    private static final String CORRECTED_XML = "correct.xml";

    private static final String XSL_FILE = "transform.xsl";

    private static TransformerFactory transformerFactory = TransformerFactory.newInstance();

    @Value("${exchange.url}")
    private String url;

    @Value("classpath:" + XSL_FILE)
    Resource resourceFile;

    private RestTemplate restTemplate;

    private CubeRepository cubeRepository;

    @Autowired
    public ScheduledTasks(RestTemplate restTemplate, CubeRepository cubeRepository) {
        this.restTemplate = restTemplate;
        this.cubeRepository = cubeRepository;
    }

    //every day at 1:00 am
    @Scheduled(cron = "0 0 1 ? * *")
    public void loadAndSaveCurrencies() {
        try {
            String response = restTemplate.getForObject(url, String.class);
            if (response != null) {
                StreamSource styleSource = new StreamSource(resourceFile.getInputStream());
                File file = new File(CORRECTED_XML);
                StreamResult correctedXML = new StreamResult(file);
                Transformer correctedTransformer = transformerFactory.newTransformer(styleSource);
                correctedTransformer.transform(new StreamSource(new StringReader(response)), correctedXML);
                JAXBContext jaxbContext = JAXBContext.newInstance(Cubes.class);
                Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                Cubes cubes = (Cubes) unmarshaller.unmarshal(file);
                for (Cube cube: cubes.getCubes()) {
                    cubeRepository.save(cube);
                    logger.info("entity {} was saved", cube);
                }
            }
        } catch (TransformerException | IOException | JAXBException pce) {
            logger.error("job error {}", pce);
            pce.printStackTrace();

        }
        logger.info("Job finished at {}", LocalDateTime.now().format(dateFormat));
    }
}
