package by.scand.hryharenka.currency.domain.repository;

import by.scand.hryharenka.currency.domain.Cube;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CubeRepository extends CrudRepository<Cube, Long> {
    Cube findFirstCubeByCurrencyOrderByIdDesc(String currency);
}
