package by.scand.hryharenka.currency.domain;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.math.BigDecimal;

@Entity(name = "cube")
@XmlAccessorType(XmlAccessType.FIELD)
public class Cube {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @XmlAttribute(name = "currency")
    private String currency;
    @Column(columnDefinition="Decimal(19,4)")
    @XmlAttribute(name = "rate")
    private BigDecimal rate;
    @Column
    @XmlAttribute(name = "time")
    private String time;


    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String value) {
        this.currency = value;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal value) {
        this.rate = value;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String value) {
        this.time = value;
    }

    @Override
    public String toString() {
        return "Cube{" +
                "id=" + id +
                ", currency='" + currency + '\'' +
                ", rate=" + rate +
                ", time='" + time + '\'' +
                '}';
    }
}
