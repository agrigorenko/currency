package by.scand.hryharenka.currency.rest.service;

import by.scand.hryharenka.currency.rest.dto.ResponseDto;

public interface ResponseService {

    ResponseDto exchange(String from, String to, long amount);
}
