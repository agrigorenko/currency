package by.scand.hryharenka.currency.rest.service.imp;

import by.scand.hryharenka.currency.ScheduledTasks;
import by.scand.hryharenka.currency.domain.Cube;
import by.scand.hryharenka.currency.domain.repository.CubeRepository;
import by.scand.hryharenka.currency.rest.dto.ResponseDto;
import by.scand.hryharenka.currency.rest.service.ResponseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
@Transactional
public class ResponseServiceImpl implements ResponseService {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);

    private CubeRepository cubeRepository;

    @Autowired
    public ResponseServiceImpl(CubeRepository cubeRepository) {
        this.cubeRepository = cubeRepository;
    }

    public ResponseDto exchange(String from, String to, long amount) {
        String time = formatter.format(LocalDate.now());
        Cube cubeFrom = cubeRepository.findFirstCubeByCurrencyOrderByIdDesc(from.toUpperCase());
        Cube cubeTo = cubeRepository.findFirstCubeByCurrencyOrderByIdDesc(to.toUpperCase());
        double result = (cubeTo.getRate().doubleValue() * amount) / cubeFrom.getRate().doubleValue();
        ResponseDto responseDto = new ResponseDto(from, amount, to, result, LocalDate.now());
        logger.info("exchange {} at {}", responseDto, time);
        return responseDto;
    }
}
