package by.scand.hryharenka.currency.rest.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);

    private static final String PREFIX_MESSAGE = "calculating with next parameters not acceptable:";

    private static final String POSTFIX_MESSAGE = " .Please check parameters and try again!";

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<Object> handle(RuntimeException ex, WebRequest request) {
        Map<String, String[]> parameterMap = request.getParameterMap();
        String message = PREFIX_MESSAGE + convertParamMapToStringWithStream(parameterMap) + POSTFIX_MESSAGE;
        logger.error("request error {}", ex);
        return handleExceptionInternal(ex, new CalculatingException(message), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    private <K, V> String convertParamMapToStringWithStream(Map<K, V> map) {
        return map.keySet().stream()
                .map(key -> key + "=" + ((String[]) map.get(key))[0])
                .collect(Collectors.joining(", ", "{", "}"));
    }

}
