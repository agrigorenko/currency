package by.scand.hryharenka.currency.rest.contoller;

import by.scand.hryharenka.currency.rest.dto.ResponseDto;
import by.scand.hryharenka.currency.rest.service.ResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResponseController {

    private ResponseService responseService;

    @Autowired
    public ResponseController(ResponseService responseService) {
        this.responseService = responseService;
    }

    @GetMapping("/exchange")
    public ResponseDto greeting(@RequestParam(value = "from") String from, @RequestParam(value = "amount") long amount, @RequestParam(value = "to") String to) {
        return responseService.exchange(from, to, amount);
    }
}