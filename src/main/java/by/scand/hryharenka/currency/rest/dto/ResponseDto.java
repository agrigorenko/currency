package by.scand.hryharenka.currency.rest.dto;

import java.time.LocalDate;

public class ResponseDto {
    private String from;
    private double amount;
    private String to;
    private double result;
    private LocalDate date;

    public ResponseDto(String from, double amount, String to, double result, LocalDate date) {
        this.from = from;
        this.amount = amount;
        this.to = to;
        this.result = result;
        this.date = date;
    }


    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "ResponseDto{" +
                "from='" + from + '\'' +
                ", amount=" + amount +
                ", to='" + to + '\'' +
                ", result=" + result +
                ", date=" + date +
                '}';
    }
}
