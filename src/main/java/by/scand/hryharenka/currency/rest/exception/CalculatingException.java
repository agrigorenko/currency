package by.scand.hryharenka.currency.rest.exception;

public class CalculatingException {
    private String message;

    public CalculatingException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
